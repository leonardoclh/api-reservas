package com.leonardoclh.reservas.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @GetMapping
    public String getAll() {
        return "{\"users\":[{\"name\":\"Leonardo\", \"country\":\"Brazil\"}," +
                "{\"name\":\"Lucas\",\"country\":\"China\"}]}";
    }

}

package com.leonardoclh.reservas.api.core.auth;

import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.Date;

public class AuthTokenService {


    static final long EXPIRACAO =  860_000_000; // 10 dias
    static final String SECRET = "secret";
    static final String TOKEN_PREFIX = "Bearer";
    static final String HEADER_STRING = "Authorization";

    static void add(HttpServletResponse response, String usuario) {

        String token = Jwts.builder()
                .setSubject(usuario)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRACAO))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();

        response.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + token);
    }

    static Authentication get(HttpServletRequest request) {

        String token = request.getHeader(HEADER_STRING);

        if (token != null) {
            String usuario = Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                    .getBody()
                    .getSubject();

            if (usuario != null) {
                return new UsernamePasswordAuthenticationToken(usuario, null, Collections.emptyList());
            }
        }
        return null;
    }
}
